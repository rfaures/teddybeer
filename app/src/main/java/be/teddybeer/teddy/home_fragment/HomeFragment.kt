package be.teddybeer.teddy.home_fragment

import android.app.Application
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.teddy.R
import be.teddybeer.teddy.data.Beer
import com.example.teddy.databinding.HomeFragmentBinding
import com.example.teddy.home_fragment.HomeFragmentDirections
import com.google.zxing.integration.android.IntentIntegrator

class HomeFragment : Fragment() {
    private lateinit var binding: HomeFragmentBinding
    private lateinit var application: Application

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?): View? {

        super.onCreateView(inflater, container, savedInstanceState)

        binding = DataBindingUtil.inflate(inflater, R.layout.home_fragment, container, false)

        val adapter = setRecyclerView()

        application = requireActivity().application
        val homeViewModel = HomeViewModel(application)
        binding.homeViewModel = homeViewModel

        setBeersListObserver(homeViewModel.beers, adapter)
        setScannedBeerObserver(homeViewModel.scannedBeer)

        binding.scanButton.setOnClickListener { getIntent() }

        homeViewModel.getAllBeersFromApi()

        return binding.root
    }

    /**
     * Returns the Intent, configured
     */
    private fun getIntent() {
        IntentIntegrator.forSupportFragment(this)
            .setOrientationLocked(false)
            .setBeepEnabled(false)
            .initiateScan()
    }

    /**
     * Lets the fragment observe the beers, notifying the adapter when changes
     */
    private fun setBeersListObserver(beers: LiveData<List<Beer>>, adapter: BeerAdapter) {
        val beersObserver = Observer<List<Beer>> { newList ->
            adapter.data = newList
        }
        beers.observe(viewLifecycleOwner, beersObserver)
    }

    /**
     * Lets the fragment observe the beers, notifying the adapter when changes
     */
    private fun setScannedBeerObserver(scannedBeer: LiveData<Beer?>) {
        val scannedBeerObserver = Observer<Beer?> { beer ->
            if (beer != null) {
                this.findNavController().navigate(
                    HomeFragmentDirections.actionHomeFragmentToBeerFragment(beer.beerId)
                )
            } else {
                Toast.makeText(application, R.string.no_beer_found, Toast.LENGTH_LONG).show()
            }
        }
        scannedBeer.observe(viewLifecycleOwner, scannedBeerObserver)
    }

    /**
     * Setting up the Recycler view
     */
    private fun setRecyclerView(): BeerAdapter {
        val adapter = BeerAdapter()
        binding.beerRecyclerView.adapter = adapter
        adapter.onItemClick = { beer ->
            this.findNavController().navigate(
                HomeFragmentDirections.actionHomeFragmentToBeerFragment(beer.beerId)
            )
        }
        return adapter
    }

    /**
     * When the scan activity finishes, passes its result to homeViewModel to be processed
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (scanResult != null) {
            if (scanResult.contents != null) {
                binding.homeViewModel!!.onScanFinished(scanResult.contents.toLong())
            } else {
                Toast.makeText(application, R.string.scan_cancelled, Toast.LENGTH_LONG).show()
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }
}