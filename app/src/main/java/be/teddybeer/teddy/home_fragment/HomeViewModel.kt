package be.teddybeer.teddy.home_fragment

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import be.teddybeer.teddy.data.Beer
import be.teddybeer.teddy.data.BeerApi
import kotlinx.coroutines.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeViewModel(application: Application) : AndroidViewModel(application) {

    private val _beers = MutableLiveData<List<Beer>>()
    val beers: LiveData<List<Beer>>
        get() = _beers

    private val _scannedBeer = MutableLiveData<Beer>()
    val scannedBeer: LiveData<Beer?>
        get() = _scannedBeer

    private var viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    /**
     * Retrieves all beers from the API, and assign their value to the LiveData.
     */
    fun getAllBeersFromApi() {
        //TODO: Don't we need a coroutine here?
        BeerApi.retrofitService.getAllBeers().enqueue(object : Callback<List<Beer>> {
            override fun onFailure(call: Call<List<Beer>>, t: Throwable) {
                //TODO: Handle this failure properly
                Toast.makeText(getApplication(), "Failure: " + t.message, Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<List<Beer>>, response: Response<List<Beer>>) {
                _beers.value = response.body()
            }
        })
    }

    /**
     * Fetches the matching beer (if any) from API, else sets _scannedBeer to null.
     */
    fun onScanFinished(barcode: Long) {
        uiScope.launch {
            BeerApi.retrofitService.searchByBarcode(barcode).enqueue(object : Callback<Beer> {
                override fun onFailure(call: Call<Beer>, t: Throwable) {
                    //TODO: Handle this failure properly
                    Toast.makeText(getApplication(), "Failure: " + t.message, Toast.LENGTH_LONG)
                        .show()
                }
                override fun onResponse(call: Call<Beer>, response: Response<Beer>) {
                    _scannedBeer.value = response.body()
                }
            })
        }
    }

    //TODO: This can move if we provide search activity
    fun searchByQuery(query: String) {
        BeerApi.retrofitService.searchByQuery(query).enqueue(object : Callback<List<Beer>> {
            override fun onFailure(call: Call<List<Beer>>, t: Throwable) {
                Toast.makeText(getApplication(), "Failure: " + t.message, Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<List<Beer>>, response: Response<List<Beer>>) {
                if (response.body() != null) {
                    _beers.value = response.body()
                }
            }
        })
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

}