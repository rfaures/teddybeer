package be.teddybeer.teddy.home_fragment

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.teddy.R
import be.teddybeer.teddy.data.Beer

class BeerAdapter : RecyclerView.Adapter<BeerAdapter.BeerViewHolder>() {
    var data = listOf<Beer>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    var onItemClick: ((Beer) -> Unit)? = null

    override fun getItemCount(): Int {
        return data.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BeerViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.beer_item_view, parent, false)
        return BeerViewHolder(view)
    }

    override fun onBindViewHolder(holder: BeerViewHolder, position: Int) {
        val item = data[position]
        holder.bind(item)
    }

    inner class BeerViewHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val beerName: TextView = itemView.findViewById(R.id.beerName)
        private val breweryName: TextView = itemView.findViewById(R.id.breweryName)
        private val style: TextView = itemView.findViewById(R.id.style)
        private val alcohol: TextView = itemView.findViewById(R.id.alcohol)

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(data[adapterPosition])
            }
        }

        fun bind(item: Beer) {
            beerName.text = item.beerInfo.beerName
            breweryName.text = item.beerInfo.breweryName
            style.text = item.beerInfo.style
            alcohol.text = item.beerInfo.alcohol

            //TODO: Load Images
        }
    }
}