package be.teddybeer.teddy.content_fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import be.teddybeer.teddy.MainActivity
import com.example.teddy.databinding.AboutUsFragmentBinding

class AboutUsFragment : Fragment() {
    private lateinit var binding: AboutUsFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        super.onCreateView(inflater, container, savedInstanceState)

        (requireActivity() as MainActivity).hideSearchMenu()

        binding = AboutUsFragmentBinding.inflate(inflater, container, false)

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        (requireActivity() as MainActivity).showSearchMenu()
    }
}