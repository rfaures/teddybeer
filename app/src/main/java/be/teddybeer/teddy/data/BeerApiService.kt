package be.teddybeer.teddy.data

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import retrofit2.http.GET
import retrofit2.http.PUT
import retrofit2.http.Query


private const val BASE_URL = "http://192.168.1.4:5000/"

enum class BeerApiFilter(val value: String) { SHOW_RENT("rent"), SHOW_BUY("buy"), SHOW_ALL("all") }

/**
 * Build the Moshi object that Retrofit will be using, making sure to add the Kotlin adapter for
 * full Kotlin compatibility.
 */
private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

/**
 * Use the Retrofit builder to build a retrofit object using a Moshi converter with our Moshi
 * object.
 */
private val retrofit = Retrofit.Builder()
    .addConverterFactory(ScalarsConverterFactory.create())
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .baseUrl(BASE_URL)
    .build()

/**
 * A public interface to expose the API methods
 */
interface BeerApiService {
    /**
     * Returns a Retrofit callback that delivers a [List] of [Beer]
     */
    @GET("beers/all")
    fun getAllBeers():
            Call<List<Beer>>

    /**
     * Returns a Retrofit callback that delivers a [List] of [Beer] from a query
     */
    @GET("beers/search")
    fun searchByQuery(@Query("text") query: String):
            Call<List<Beer>>

    /**
     * Returns a Retrofit callback that delivers a [Beer] if barcode matched
     */
    @GET("beers/by_barcode")
    fun searchByBarcode(@Query("barcode") barcode: Long):
            Call<Beer>

    /**
     * Returns a Retrofit callback that delivers a [Beer] if _id matched
     */
    @GET("beers/by_id")
    fun searchById(@Query("_id") _id: String):
            Call<Beer>

    @PUT("beers/update_barcode")
    fun updateBarcode(@Query("new") barcode: Long, @Query("_id") _id: String):
            Call<String>
}

/**
 * A public Api object that exposes the lazy-initialized Retrofit service
 */
object BeerApi {
    val retrofitService: BeerApiService by lazy {
        retrofit.create(BeerApiService::class.java)
    }
}
