package be.teddybeer.teddy.data

import com.squareup.moshi.Json

data class Beer(
    @Json(name = "_id")
    var beerId: String,
    @Json(name = "_source")
    var beerInfo: BeerInfo
)

data class BeerInfo(
    @Json(name = "name")
    val beerName: String,

    @Json(name = "brewery_name")
    var breweryName: String,

    @Json(name = "alcohol")
    var alcohol: String?,

    @Json(name = "style")
    var style: String?,

    @Json(name = "img_link")
    var img_link: String?,

    @Json(name = "barcode")
    var barcode: Long?
)