package be.teddybeer.teddy

import android.app.SearchManager
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.example.teddy.R
import com.example.teddy.databinding.ActivityMainBinding
import com.example.teddy.home_fragment.HomeFragmentDirections
import com.example.teddy.search_fragment.SearchFragmentDirections
import com.google.android.material.navigation.NavigationView


class MainActivity : AppCompatActivity() {
    private lateinit var drawerLayout: DrawerLayout
    private lateinit var binding: ActivityMainBinding
    private lateinit var navController: NavController
    private lateinit var menu: Menu

    override fun onCreate(savedInstanceState: Bundle?) {

        // Change back to normal theme from splash screen theme
        setTheme(R.style.AppTheme)

        super.onCreate(savedInstanceState)

        // Inflate and display content
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        navController = this.findNavController(R.id.nav_host_fragment)

        setAppBar()
    }

    /**
     * Inflates the Options Menu (which contains the Search Widget)
     */
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.options_menu, menu)
        this.menu = menu

        // Associate searchable configuration with the SearchView
        val searchManager = getSystemService(Context.SEARCH_SERVICE) as SearchManager
        (menu.findItem(R.id.search).actionView as SearchView).apply {
            setSearchableInfo(searchManager.getSearchableInfo(componentName))
        }

        return true
    }

    /**
     * Handles the Navigate Up button by linking it to the NavController.
     */
    override fun onSupportNavigateUp(): Boolean {
        return NavigationUI.navigateUp(navController, drawerLayout)
    }

    /**
     * Sets the app bar with a drawer view and links it to the NavController.
     */
    private fun setAppBar() {
        drawerLayout = binding.drawerLayout
        val appBarConfiguration = AppBarConfiguration(navController.graph, drawerLayout)
        setupActionBarWithNavController(navController, appBarConfiguration)

        // Link drawer view to nav controller
        findViewById<NavigationView>(R.id.navView)
            .setupWithNavController(navController)
    }

    override fun onNewIntent(intent: Intent) {
        super.onNewIntent(intent)
        var query = intent.getStringExtra(SearchManager.QUERY)
        if (query != null) {
            query = query.capitalize()
            if (navController.currentDestination?.id == R.id.homeFragment)
                navController.navigate(HomeFragmentDirections.actionHomeFragmentToSearchFragment(query))
            else if (navController.currentDestination?.id == R.id.searchFragment)
                navController.navigate(SearchFragmentDirections.actionSearchFragmentToSearchFragment(query))
            menu.findItem(R.id.search).collapseActionView()
        }
    }

    fun hideSearchMenu() {
        menu.findItem(R.id.search).isVisible = false
    }

    fun showSearchMenu() {
        menu.findItem(R.id.search).isVisible = true
    }
}

