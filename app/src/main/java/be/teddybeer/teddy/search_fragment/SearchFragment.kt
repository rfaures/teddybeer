package be.teddybeer.teddy.search_fragment

import android.app.Application
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.example.teddy.R
import be.teddybeer.teddy.data.Beer
import com.example.teddy.databinding.SearchFragmentBinding
import be.teddybeer.teddy.home_fragment.BeerAdapter
import com.example.teddy.search_fragment.SearchFragmentArgs
import com.example.teddy.search_fragment.SearchFragmentDirections

class SearchFragment : Fragment() {
    private lateinit var binding: SearchFragmentBinding
    private lateinit var application: Application

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?): View? {

        super.onCreateView(inflater, container, savedInstanceState)

        binding = DataBindingUtil.inflate(inflater, R.layout.search_fragment, container, false)

        val adapter = setRecyclerView()

        application = requireActivity().application
        val searchViewModel = SearchViewModel(application)
        binding.searchViewModel = searchViewModel

        setBeersListObserver(searchViewModel.beers, adapter)

        val query = SearchFragmentArgs.fromBundle(requireArguments()).query
        searchViewModel.searchByQuery(query)

        return binding.root
    }

    /**
     * Lets the fragment observe the beers, notifying the adapter when changes
     */
    private fun setBeersListObserver(beers: LiveData<List<Beer>>, adapter: BeerAdapter) {
        val beersObserver = Observer<List<Beer>> { newList ->
            adapter.data = newList
        }
        beers.observe(viewLifecycleOwner, beersObserver)
    }

    /**
     * Setting up the Recycler view
     */
    private fun setRecyclerView(): BeerAdapter {
        val adapter = BeerAdapter()
        binding.beerRecyclerView.adapter = adapter
        adapter.onItemClick = { beer ->
            this.findNavController().navigate(
                SearchFragmentDirections.actionSearchFragmentToBeerFragment(beer.beerId)
            )
        }
        return adapter
    }
}