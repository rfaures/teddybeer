package be.teddybeer.teddy.beer_fragment

import android.app.Application
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import be.teddybeer.teddy.MainActivity
import com.example.teddy.R
import com.example.teddy.beer_fragment.BeerFragmentArgs
import be.teddybeer.teddy.data.Beer
import com.example.teddy.databinding.BeerFragmentBinding
import com.google.zxing.integration.android.IntentIntegrator

class BeerFragment : Fragment() {
    private lateinit var binding: BeerFragmentBinding
    private lateinit var application: Application

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        binding = DataBindingUtil.inflate(inflater, R.layout.beer_fragment, container, false)

        (requireActivity() as MainActivity).hideSearchMenu()

        application = requireActivity().application
        val beerViewModel = BeerViewModel(application)
        binding.beerViewModel = beerViewModel

        val beerId = BeerFragmentArgs.fromBundle(requireArguments()).beerId

        setBeerObserver(beerViewModel)

        beerViewModel.searchById(beerId)

        setScanButton()

        return binding.root
    }

    /**
     * Handling the Scan Button
     */
    private fun setScanButton() {
        binding.scanButton.setOnClickListener {
            IntentIntegrator.forSupportFragment(this)
                .setOrientationLocked(false)
                .setBeepEnabled(false)
                .initiateScan()
        }
    }

    private fun setBeerObserver(beerViewModel: BeerViewModel) {
        val beerObserver = Observer<Beer> { beer ->
            binding.beerText.text = beer.beerInfo.beerName
            binding.breweryNameText.text = beer.beerInfo.breweryName
            if (beer.beerInfo.barcode != null)
                binding.barcodeText.text = beer.beerInfo.barcode.toString()
            if (beer.beerInfo.style != null)
                binding.styleText.text = beer.beerInfo.style
            if (beer.beerInfo.alcohol != null)
                binding.alcoholText.text = beer.beerInfo.alcohol
        }
        beerViewModel.beer.observe(viewLifecycleOwner, beerObserver)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val scanResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (scanResult != null) {
            if (scanResult.contents != null) {
                binding.beerViewModel!!.onScanFinished(scanResult.contents.toLong())
            } else {
                Toast.makeText(application, R.string.scan_cancelled, Toast.LENGTH_LONG).show()
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        (requireActivity() as MainActivity).showSearchMenu()
    }
}