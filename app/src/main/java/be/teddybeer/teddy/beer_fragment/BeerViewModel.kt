package be.teddybeer.teddy.beer_fragment

import android.app.Application
import android.widget.Toast
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import be.teddybeer.teddy.data.Beer
import be.teddybeer.teddy.data.BeerApi
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BeerViewModel(application: Application) : AndroidViewModel(application) {

    private val _beer = MutableLiveData<Beer>()
    val beer: LiveData<Beer>
        get() = _beer

    private var viewModelJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + viewModelJob)

    /**
     * Fetches the matching beer (if any) from API, else sets _scannedBeer to null.
     */
    fun onScanFinished(barcode: Long) {
        uiScope.launch {
            BeerApi.retrofitService.updateBarcode(barcode, _beer.value!!.beerId).enqueue(object : Callback<String> {
                override fun onFailure(call: Call<String>, t: Throwable) {
                    //TODO: Handle this failure properly
                    Toast.makeText(getApplication(), "Failure: " + t.message, Toast.LENGTH_LONG)
                        .show()
                }
                override fun onResponse(call: Call<String>, response: Response<String>) {
                    searchById(_beer.value!!.beerId)
                }
            })
        }
    }

    fun searchById(id: String) {
        BeerApi.retrofitService.searchById(id).enqueue(object : Callback<Beer> {
            override fun onFailure(call: Call<Beer>, t: Throwable) {
                Toast.makeText(getApplication(), "Failure: " + t.message, Toast.LENGTH_LONG).show()
            }

            override fun onResponse(call: Call<Beer>, response: Response<Beer>) {
                if (response.body() != null) {
                    _beer.value = response.body()
                } else {
                    Toast.makeText(getApplication(), "No beer found", Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    override fun onCleared() {
        super.onCleared()
        viewModelJob.cancel()
    }

}